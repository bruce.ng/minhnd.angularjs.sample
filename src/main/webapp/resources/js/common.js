/**
 * Created by Administrator on 10/9/2015.
 */
/** create module: common*/
angular.module('common', ['ngMessages']).
    controller('BaseFormCtrl', ['$scope', '$http', function($scope, $http){

        var fieldWithFocus;

        $scope.vm = {
            submitted: false,
            errorMessages: []
        };

        $scope.preparePostData = function() {
            var username = $scope.vm.username != undefined ? $scope.vm.username : '';
            var password = $scope.vm.password != undefined ? $scope.vm.password : '';
            //var email = $scope.vm.email != undefined ? $scope.vm.email : '';

            //return 'username=' + username + '&password=' + password + '&email=' + email;
            return 'username=' + username + '&password=' + password;
        }

        $scope.login = function(username, password) {
            var postData = $scope.preparePostData();
            $http({
                method: 'POST',
                uri: '/authenticate',
                data: postData,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "X-Login-Ajax-call": 'true'
                }
            })
        }
    }])

/**
 * The `ngMessages` module provides enhanced support for displaying messages within templates
 * */
