package config;

import config.root.AppSecurityConfig;
import config.root.DevelopmentConfiguration;
import config.root.RootContextConfig;
import config.root.TestConfiguration;
import config.servlet.ServletContextConfig;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Created by Administrator on 10/7/2015.
 * Replacement for most of the content of web.xml, sets up the root and the servlet context config
 */
public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{
                RootContextConfig.class, DevelopmentConfiguration.class, TestConfiguration.class, AppSecurityConfig.class
        };

    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[]{ServletContextConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
